#!/bin/bash

./keycloak-action.sh inject-theme -e theme=test-theme.d4science.org -e 'title_tag="test gateway"' -e logo_url=https://dev.d4science.org/image/layout_set_logo?img_id=36727858 -e logo_alt="test gateway" -e project_url=http://www.google.it -e project_description="A test project for a test theme" -e EC_logo=yes
