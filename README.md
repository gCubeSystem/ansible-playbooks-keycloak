# ansible-playbooks-keycloak

The playbooks for managing Keycloak setup. Currently the following playbooks are supported:

1. inject-theme: an action to generate a theme for a typical D4Science gateway. This playbook depends on the Ansible role ansible-role-keycloak-theme.



Examples:

`inject-test-theme.sh` and `inject-production-themes.sh` contain example calls to the playbook with different customizations.

The following example generates an example theme that overrides most of the customizable fields.

`./keycloak-action.sh inject-theme -e 'favicon_url="https://aginfra.d4science.org/generic-configurable-theme/images/favicon.ico"' -e 'infrastructure_logo="yes"' -e 'theme="aginfra.d4science.org"' -e 'project_url="http://plus.aginfra.eu/"' -e 'background_image=""' -e 'EC_logo="yes"' -e 'title_tag="AGINFRAPlus Gateway"' -e 'logo_alt="AGINFRAPlus Gateway"' -e 'project_description=""' -e 'terms_url="https://aginfra.d4science.org/terms-of-use"' -e 'privacy_policy_url="https://www.iubenda.com/privacy-policy/441050"' -e 'logo_url="https://aginfra.d4science.org/image/layout_set_logo?img_id=26845094&t=1610441736518"' -e 'footer="AGINFRA PLUS receives funding from the European Union''s Horizon 2020 research and innovation programme under grant agreement No. 731001 <br/> The views and opinions expressed in this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission."' -e 'cookie_policy_url="https://aginfra.d4science.org/cookie-policy"'`

This call translates to the following calls:

`ansible-galaxy install --force-with-deps -r inject-theme/requirements.yaml`
`ansible-playbook inject-theme/playbook.yaml -e 'favicon_url="https://aginfra.d4science.org/generic-configurable-theme/images/favicon.ico"' -e 'infrastructure_logo="yes"' -e 'theme="aginfra.d4science.org"' -e 'project_url="http://plus.aginfra.eu/"' -e 'background_image=""' -e 'EC_logo="yes"' -e 'title_tag="AGINFRAPlus Gateway"' -e 'logo_alt="AGINFRAPlus Gateway"' -e 'project_description=""' -e 'terms_url="https://aginfra.d4science.org/terms-of-use"' -e 'privacy_policy_url="https://www.iubenda.com/privacy-policy/441050"' -e 'logo_url="https://aginfra.d4science.org/image/layout_set_logo?img_id=26845094&t=1610441736518"' -e 'footer="AGINFRA PLUS receives funding from the European Union''s Horizon 2020 research and innovation programme under grant agreement No. 731001 <br/> The views and opinions expressed in this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission."' -e 'cookie_policy_url="https://aginfra.d4science.org/cookie-policy"'`
